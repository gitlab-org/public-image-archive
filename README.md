# public-image-archive

The `public-image-archive` project hosts official GitLab container images within the [container registry](https://gitlab.com/gitlab-org/public-image-archive/container_registry). These images and more are also available on DockerHub within the [GitLab repositiory on DockerHub](https://hub.docker.com/u/gitlab).

## Requesting an image and version be added to the archive

DockerHub is the default location for distributing several of GitLab's images. We are adding specific older versions to this archive based on demand. If there is a version on [GitLab's DockerHub repo](https://hub.docker.com/u/gitlab) respository that you would like copied here, please [open an issue in the public-image-acrhive project](https://gitlab.com/gitlab-org/public-image-archive/-/issues) and request the image and version.

A potential future change is to [automatically uploading new GitLab images to a registry here on gitlab.com](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/7459).


## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).


## Licenses

For the git repository see the [LICENSE](LICENSE)


